import React, { Component } from "react";
import { Gradient } from "react-gradient";
import ReactMapboxGl, { ZoomControl, Marker } from "react-mapbox-gl";
import "./App.css";

const Map = ReactMapboxGl({
  accessToken:
    "pk.eyJ1IjoibWFydG9uc3phYm9sY3MiLCJhIjoiY2pqeTVlZXVuMzJqbTNwbnVkYWVnYWV1eiJ9.BHFQuVnMv9w09SNDDfov6w"
});

const gradients = [["#191A1A", "#C1C1C1"], ["#333332", "#454545"]];

export default class MapView extends Component {
  constructor() {
    super();
    this.state = {
      data: []
    };
  }
  componentDidMount() {
    this._interval = setInterval(() => {
      this.download();
    }, 5000);
  }
  componentWillUnmount() {
    clearInterval(this._interval);
  }

  download() {
    fetch("https://trackingserverszabi.herokuapp.com/places")
      .then(response => response.json())
      .then(data => {
        console.log(data);
        var numbers;
        data.map((value, i) => {
          numbers = value.coordinates.split(",");
          data[i].places = [Number(numbers[1]), Number(numbers[0])];
        });
        this.setState({ data, current:data[0].places });
      });
  }

  render() {
    return (
      <Gradient
        gradients={gradients} // required
        property="background"
        duration={0}
        angle="45deg"
      >
        <div className="App">
          <header className="App-header" />
          <body className="App-body">
            <p className="map">
              <Map
                center={this.state.current}
                style="mapbox://styles/martonszabolcs/cjjy4za247pbd2snpbliympld"
                containerStyle={{
                  height: "100vh",
                  borderRadius: 10,
                  width: "100vw",
                  position: "absolute",
                  zIndex: 100,
                  left: 0,
                  top: 0
                }}
              >
                <ZoomControl />
                {this.state.data.map((venue, index) => {
                  return (
                    <Marker
                      key={index}
                      coordinates={venue.places}
                      anchor="bottom"
                    >
                      <img
                        style={{ zIndex: 1 }}
                        src="//api.tiles.mapbox.com/mapbox.js/v2.3.0/images/marker-icon.png"
                      />
                    </Marker>
                  );
                })}
              </Map>
            </p>
          </body>
        </div>
      </Gradient>
    );
  }
}
